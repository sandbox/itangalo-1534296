<?php
/**
 * @file
 * scorg_unique_field.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function scorg_unique_field_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
