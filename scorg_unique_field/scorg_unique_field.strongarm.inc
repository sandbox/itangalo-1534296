<?php
/**
 * @file
 * scorg_unique_field.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function scorg_unique_field_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'unique_field_comp_sc_resource_external';
  $strongarm->value = 'each';
  $export['unique_field_comp_sc_resource_external'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'unique_field_comp_sc_topic';
  $strongarm->value = 'each';
  $export['unique_field_comp_sc_topic'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'unique_field_fields_sc_resource_external';
  $strongarm->value = array(
    0 => 'sc_externalresource_link',
  );
  $export['unique_field_fields_sc_resource_external'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'unique_field_fields_sc_topic';
  $strongarm->value = array(
    0 => 'title',
  );
  $export['unique_field_fields_sc_topic'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'unique_field_scope_sc_resource_external';
  $strongarm->value = 'type';
  $export['unique_field_scope_sc_resource_external'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'unique_field_scope_sc_topic';
  $strongarm->value = 'type';
  $export['unique_field_scope_sc_topic'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'unique_field_show_matches_sc_resource_external';
  $strongarm->value = array(
    0 => 'show_matches',
  );
  $export['unique_field_show_matches_sc_resource_external'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'unique_field_show_matches_sc_topic';
  $strongarm->value = array(
    0 => 'show_matches',
  );
  $export['unique_field_show_matches_sc_topic'] = $strongarm;

  return $export;
}
