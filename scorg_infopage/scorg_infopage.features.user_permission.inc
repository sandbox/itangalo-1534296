<?php
/**
 * @file
 * scorg_infopage.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function scorg_infopage_user_default_permissions() {
  $permissions = array();

  // Exported permission: create infopage content
  $permissions['create infopage content'] = array(
    'name' => 'create infopage content',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any infopage content
  $permissions['delete any infopage content'] = array(
    'name' => 'delete any infopage content',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own infopage content
  $permissions['delete own infopage content'] = array(
    'name' => 'delete own infopage content',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any infopage content
  $permissions['edit any infopage content'] = array(
    'name' => 'edit any infopage content',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own infopage content
  $permissions['edit own infopage content'] = array(
    'name' => 'edit own infopage content',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'node',
  );

  return $permissions;
}
