<?php
/**
 * @file
 * scorg_infopage.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function scorg_infopage_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function scorg_infopage_node_info() {
  $items = array(
    'infopage' => array(
      'name' => t('Information page'),
      'base' => 'node_content',
      'description' => t('Information pages contain static information, such as "about us".'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
