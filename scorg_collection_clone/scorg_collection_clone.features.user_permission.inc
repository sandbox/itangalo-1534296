<?php
/**
 * @file
 * scorg_collection_clone.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function scorg_collection_clone_user_default_permissions() {
  $permissions = array();

  // Exported permission: clone own nodes.
  $permissions['clone own nodes'] = array(
    'name' => 'clone own nodes',
    'roles' => array(
      0 => 'Green belt',
      1 => 'administrator',
    ),
    'module' => 'clone',
  );

  return $permissions;
}
