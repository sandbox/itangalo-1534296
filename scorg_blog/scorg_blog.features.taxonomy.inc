<?php
/**
 * @file
 * scorg_blog.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function scorg_blog_taxonomy_default_vocabularies() {
  return array(
    'categories' => array(
      'name' => 'Categories',
      'machine_name' => 'categories',
      'description' => 'Categories are used as tags for blog posts.',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
      'microdata' => array(
        '#attributes' => array(
          'itemscope' => '',
        ),
        'sc_topic_description' => array(
          '#attributes' => array(),
        ),
        'sc_topic_isapartof' => array(
          '#attributes' => array(),
        ),
        'sc_topic_requires' => array(
          '#attributes' => array(),
        ),
        'sc_topic_isapartof_strength' => array(
          '#attributes' => array(),
        ),
        'sc_topic_requires_strength' => array(
          '#attributes' => array(),
        ),
        'sc_topicrelation_strength' => array(
          '#attributes' => array(),
        ),
        'comment_body' => array(
          '#attributes' => array(),
        ),
        'sc_externalresource_link' => array(
          '#attributes' => array(),
        ),
        'sc_resource_description' => array(
          '#attributes' => array(),
        ),
        'sc_resource_topics' => array(
          '#attributes' => array(),
        ),
        'sc_resource_vote_clear' => array(
          '#attributes' => array(),
        ),
        'sc_resource_vote_correct' => array(
          '#attributes' => array(),
        ),
        'sc_resource_vote_entertaining' => array(
          '#attributes' => array(),
        ),
        'levelup_points' => array(
          '#attributes' => array(),
        ),
        'field_levelup_description' => array(
          '#attributes' => array(),
        ),
        'web_tid' => array(
          '#attributes' => array(),
        ),
        'field_drupal_project' => array(
          '#attributes' => array(),
        ),
        'body' => array(
          '#attributes' => array(),
        ),
        'field_blog_categories' => array(
          '#attributes' => array(),
        ),
        'field_blog_image' => array(
          '#attributes' => array(),
        ),
        'field_blog_promote' => array(
          '#attributes' => array(),
        ),
        'field_infopage_image' => array(
          '#attributes' => array(),
        ),
      ),
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
