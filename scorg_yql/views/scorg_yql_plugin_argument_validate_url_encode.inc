<?php

/**
 * @file
 * Converts a view argument to its URL encoded equivalent.
 */

/**
 * Validate that an argument is set, and if so create a URL encoded version.
 */
class scorg_yql_plugin_argument_validate_url_encode extends views_plugin_argument_validate {
  function validate_argument($argument) {
    if (!isset($argument)) {
      return FALSE;
    }

    $this->argument->validated_title = urlencode($argument);
    return TRUE;
  }
}
