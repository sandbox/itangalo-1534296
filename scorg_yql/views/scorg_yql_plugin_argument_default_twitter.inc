<?php

/**
 * @file
 * A default argument loader to get a twitter tag for the viewed node.
 */

/**
 * Argument plugin to extract a node via arg (sorry) and format as a tweet tag.
 */
class scorg_yql_plugin_argument_default_twitter extends views_plugin_argument_default {
  function get_argument() {
    if (arg(0) == 'node' && is_numeric(arg(1))) {
      $nid = arg(1);

      // Also check if we're viewing a resource in the context of a collection.
      if (is_numeric(arg(2))) {
        $nid = arg(2);
      }
      
      return '#scorg-' . $nid;
    }
  }
}
