<?php

/**
 * @file
 * Declares a default argument plugin, used for YQL and Twitter integration.
 */

function scorg_yql_views_plugins() {
  $plugins = array(
    'argument default' => array(
      'scorg_twitter' => array(
        'title' => t('Viewed content as Twitter tag'),
        'handler' => 'scorg_yql_plugin_argument_default_twitter',
      ),
    ),
    'argument validator' => array(
      'scorg_url_encode' => array(
        'title' => t('URL encode'),
        'handler' => 'scorg_yql_plugin_argument_validate_url_encode',
      ),
    ),
  );
  return $plugins;
}
