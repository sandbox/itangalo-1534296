<?php
/**
 * @file
 * scorg_yql.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function scorg_yql_views_default_views() {
  $export = array();

  $view = new view;
  $view->name = 'scorg_google_search';
  $view->description = 'A list of Google search results for a given topic.';
  $view->tag = 'default';
  $view->base_table = 'yql';
  $view->human_name = 'Google search';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['yql_base_table'] = 'google.search';
  $handler->display->display_options['query']['options']['num_items'] = '10';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'column' => 'column',
    'column_1' => 'column_1',
    'column_2' => 'column_2',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'column' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'column_1' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'column_2' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['override'] = 1;
  $handler->display->display_options['style_options']['sticky'] = 0;
  $handler->display->display_options['style_options']['empty_table'] = 0;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['label'] = 'Empty text';
  $handler->display->display_options['empty']['area']['empty'] = FALSE;
  $handler->display->display_options['empty']['area']['content'] = 'It seems Google can\'t find anthing on this topic. :-(';
  $handler->display->display_options['empty']['area']['format'] = 'filtered_markdown';
  $handler->display->display_options['empty']['area']['tokenize'] = 0;
  /* Field: Global: View result counter */
  $handler->display->display_options['fields']['counter']['id'] = 'counter';
  $handler->display->display_options['fields']['counter']['table'] = 'views';
  $handler->display->display_options['fields']['counter']['field'] = 'counter';
  $handler->display->display_options['fields']['counter']['label'] = '';
  $handler->display->display_options['fields']['counter']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['counter']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['counter']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['counter']['alter']['external'] = 0;
  $handler->display->display_options['fields']['counter']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['counter']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['counter']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['counter']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['counter']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['counter']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['counter']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['counter']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['counter']['alter']['html'] = 0;
  $handler->display->display_options['fields']['counter']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['counter']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['counter']['hide_empty'] = 0;
  $handler->display->display_options['fields']['counter']['empty_zero'] = 0;
  $handler->display->display_options['fields']['counter']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['counter']['counter_start'] = '1';
  /* Field: Link in plain text */
  $handler->display->display_options['fields']['column']['id'] = 'column';
  $handler->display->display_options['fields']['column']['table'] = 'yql';
  $handler->display->display_options['fields']['column']['field'] = 'column';
  $handler->display->display_options['fields']['column']['ui_name'] = 'Link in plain text';
  $handler->display->display_options['fields']['column']['label'] = '';
  $handler->display->display_options['fields']['column']['exclude'] = TRUE;
  $handler->display->display_options['fields']['column']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['column']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['column']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['column']['alter']['external'] = 0;
  $handler->display->display_options['fields']['column']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['column']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['column']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['column']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['column']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['column']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['column']['alter']['strip_tags'] = 1;
  $handler->display->display_options['fields']['column']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['column']['alter']['html'] = 0;
  $handler->display->display_options['fields']['column']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['column']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['column']['hide_empty'] = 0;
  $handler->display->display_options['fields']['column']['empty_zero'] = 0;
  $handler->display->display_options['fields']['column']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['column']['field_name'] = 'unescapedUrl';
  /* Field: Content */
  $handler->display->display_options['fields']['column_1']['id'] = 'column_1';
  $handler->display->display_options['fields']['column_1']['table'] = 'yql';
  $handler->display->display_options['fields']['column_1']['field'] = 'column';
  $handler->display->display_options['fields']['column_1']['ui_name'] = 'Content';
  $handler->display->display_options['fields']['column_1']['label'] = 'Description';
  $handler->display->display_options['fields']['column_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['column_1']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['column_1']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['column_1']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['column_1']['alter']['external'] = 0;
  $handler->display->display_options['fields']['column_1']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['column_1']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['column_1']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['column_1']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['column_1']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['column_1']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['column_1']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['column_1']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['column_1']['alter']['html'] = 0;
  $handler->display->display_options['fields']['column_1']['element_label_colon'] = 0;
  $handler->display->display_options['fields']['column_1']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['column_1']['hide_empty'] = 0;
  $handler->display->display_options['fields']['column_1']['empty_zero'] = 0;
  $handler->display->display_options['fields']['column_1']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['column_1']['field_name'] = 'content';
  /* Field: Linked title */
  $handler->display->display_options['fields']['column_2']['id'] = 'column_2';
  $handler->display->display_options['fields']['column_2']['table'] = 'yql';
  $handler->display->display_options['fields']['column_2']['field'] = 'column';
  $handler->display->display_options['fields']['column_2']['ui_name'] = 'Linked title';
  $handler->display->display_options['fields']['column_2']['label'] = 'Resource (hover for description)';
  $handler->display->display_options['fields']['column_2']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['column_2']['alter']['make_link'] = 1;
  $handler->display->display_options['fields']['column_2']['alter']['path'] = '[column]';
  $handler->display->display_options['fields']['column_2']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['column_2']['alter']['external'] = 1;
  $handler->display->display_options['fields']['column_2']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['column_2']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['column_2']['alter']['alt'] = '[column_1]';
  $handler->display->display_options['fields']['column_2']['alter']['target'] = '_blank';
  $handler->display->display_options['fields']['column_2']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['column_2']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['column_2']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['column_2']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['column_2']['alter']['strip_tags'] = 1;
  $handler->display->display_options['fields']['column_2']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['column_2']['alter']['html'] = 0;
  $handler->display->display_options['fields']['column_2']['element_label_colon'] = 0;
  $handler->display->display_options['fields']['column_2']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['column_2']['hide_empty'] = 0;
  $handler->display->display_options['fields']['column_2']['empty_zero'] = 0;
  $handler->display->display_options['fields']['column_2']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['column_2']['field_name'] = 'titleNoFormatting';
  /* Contextual filter: Query string */
  $handler->display->display_options['arguments']['column']['id'] = 'column';
  $handler->display->display_options['arguments']['column']['table'] = 'yql';
  $handler->display->display_options['arguments']['column']['field'] = 'column';
  $handler->display->display_options['arguments']['column']['ui_name'] = 'Query string';
  $handler->display->display_options['arguments']['column']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['column']['title_enable'] = 1;
  $handler->display->display_options['arguments']['column']['title'] = 'Top Google results for "%1"';
  $handler->display->display_options['arguments']['column']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['column']['default_argument_skip_url'] = 0;
  $handler->display->display_options['arguments']['column']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['column']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['column']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['column']['transform_dash'] = 0;
  $handler->display->display_options['arguments']['column']['field_name'] = 'q';

  /* Display: Content pane */
  $handler = $view->new_display('panel_pane', 'Content pane', 'panel_pane_1');
  $handler->display->display_options['pane_title'] = 'Google search for topic';
  $handler->display->display_options['pane_category']['name'] = 'Skill Compass: Learn Drupal';
  $handler->display->display_options['pane_category']['weight'] = '0';
  $handler->display->display_options['argument_input'] = array(
    'column' => array(
      'type' => 'user',
      'context' => 'entity:comment.author',
      'context_optional' => 0,
      'panel' => '0',
      'fixed' => '',
      'label' => 'Query string',
    ),
  );
  $export['scorg_google_search'] = $view;

  $view = new view;
  $view->name = 'scorg_tweets';
  $view->description = 'A list of Tweet search results for a given node.';
  $view->tag = 'default';
  $view->base_table = 'yql';
  $view->human_name = 'skillcompass.org tweets';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Comments on Twitter';
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['yql_base_table'] = 'twitter.search';
  $handler->display->display_options['query']['options']['num_items'] = '10';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['row_class'] = 'scorg-tweets';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['inline'] = array(
    'nothing' => 'nothing',
    'column_3' => 'column_3',
    'column' => 'column',
  );
  $handler->display->display_options['row_options']['hide_empty'] = 0;
  $handler->display->display_options['row_options']['default_field_elements'] = 1;
  /* Footer: Global: Text area */
  $handler->display->display_options['footer']['area']['id'] = 'area';
  $handler->display->display_options['footer']['area']['table'] = 'views';
  $handler->display->display_options['footer']['area']['field'] = 'area';
  $handler->display->display_options['footer']['area']['label'] = 'Twitter link';
  $handler->display->display_options['footer']['area']['empty'] = TRUE;
  $handler->display->display_options['footer']['area']['content'] = 'You can leave a comment by [tweeting with the tag !1](http://twitter.com/home?status=%1).';
  $handler->display->display_options['footer']['area']['format'] = 'full_markdown';
  $handler->display->display_options['footer']['area']['tokenize'] = 1;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['label'] = 'Empty text';
  $handler->display->display_options['empty']['area']['empty'] = FALSE;
  $handler->display->display_options['empty']['area']['content'] = 'No recent tweets for this page found.';
  $handler->display->display_options['empty']['area']['format'] = 'filtered_markdown';
  $handler->display->display_options['empty']['area']['tokenize'] = 0;
  /* Field: From user */
  $handler->display->display_options['fields']['column_1']['id'] = 'column_1';
  $handler->display->display_options['fields']['column_1']['table'] = 'yql';
  $handler->display->display_options['fields']['column_1']['field'] = 'column';
  $handler->display->display_options['fields']['column_1']['ui_name'] = 'From user';
  $handler->display->display_options['fields']['column_1']['label'] = '';
  $handler->display->display_options['fields']['column_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['column_1']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['column_1']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['column_1']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['column_1']['alter']['external'] = 0;
  $handler->display->display_options['fields']['column_1']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['column_1']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['column_1']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['column_1']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['column_1']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['column_1']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['column_1']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['column_1']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['column_1']['alter']['html'] = 0;
  $handler->display->display_options['fields']['column_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['column_1']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['column_1']['hide_empty'] = 0;
  $handler->display->display_options['fields']['column_1']['empty_zero'] = 0;
  $handler->display->display_options['fields']['column_1']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['column_1']['field_name'] = 'from_user';
  /* Field: Profile image url */
  $handler->display->display_options['fields']['column_2']['id'] = 'column_2';
  $handler->display->display_options['fields']['column_2']['table'] = 'yql';
  $handler->display->display_options['fields']['column_2']['field'] = 'column';
  $handler->display->display_options['fields']['column_2']['ui_name'] = 'Profile image url';
  $handler->display->display_options['fields']['column_2']['label'] = '';
  $handler->display->display_options['fields']['column_2']['exclude'] = TRUE;
  $handler->display->display_options['fields']['column_2']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['column_2']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['column_2']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['column_2']['alter']['external'] = 0;
  $handler->display->display_options['fields']['column_2']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['column_2']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['column_2']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['column_2']['alter']['max_length'] = '200';
  $handler->display->display_options['fields']['column_2']['alter']['word_boundary'] = 0;
  $handler->display->display_options['fields']['column_2']['alter']['ellipsis'] = 0;
  $handler->display->display_options['fields']['column_2']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['column_2']['alter']['strip_tags'] = 1;
  $handler->display->display_options['fields']['column_2']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['column_2']['alter']['html'] = 0;
  $handler->display->display_options['fields']['column_2']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['column_2']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['column_2']['hide_empty'] = 0;
  $handler->display->display_options['fields']['column_2']['empty_zero'] = 0;
  $handler->display->display_options['fields']['column_2']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['column_2']['field_name'] = 'profile_image_url';
  /* Field: Linked image */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['ui_name'] = 'Linked image';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<img src="[column_2]" />';
  $handler->display->display_options['fields']['nothing']['alter']['make_link'] = 1;
  $handler->display->display_options['fields']['nothing']['alter']['path'] = 'http://twitter.com/[column_1]';
  $handler->display->display_options['fields']['nothing']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['external'] = 1;
  $handler->display->display_options['fields']['nothing']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['alt'] = '[column_1]';
  $handler->display->display_options['fields']['nothing']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['nothing']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['nothing']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['html'] = 0;
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['nothing']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['nothing']['hide_empty'] = 0;
  $handler->display->display_options['fields']['nothing']['empty_zero'] = 0;
  $handler->display->display_options['fields']['nothing']['hide_alter_empty'] = 0;
  /* Field: Tweet id */
  $handler->display->display_options['fields']['column_3']['id'] = 'column_3';
  $handler->display->display_options['fields']['column_3']['table'] = 'yql';
  $handler->display->display_options['fields']['column_3']['field'] = 'column';
  $handler->display->display_options['fields']['column_3']['ui_name'] = 'Tweet id';
  $handler->display->display_options['fields']['column_3']['label'] = '';
  $handler->display->display_options['fields']['column_3']['alter']['alter_text'] = 1;
  $handler->display->display_options['fields']['column_3']['alter']['text'] = '(link)';
  $handler->display->display_options['fields']['column_3']['alter']['make_link'] = 1;
  $handler->display->display_options['fields']['column_3']['alter']['path'] = 'http://twitter.com/[column_1]/status/[column_3]';
  $handler->display->display_options['fields']['column_3']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['column_3']['alter']['external'] = 1;
  $handler->display->display_options['fields']['column_3']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['column_3']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['column_3']['alter']['alt'] = 'Tweet from [column_1]';
  $handler->display->display_options['fields']['column_3']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['column_3']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['column_3']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['column_3']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['column_3']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['column_3']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['column_3']['alter']['html'] = 0;
  $handler->display->display_options['fields']['column_3']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['column_3']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['column_3']['hide_empty'] = 0;
  $handler->display->display_options['fields']['column_3']['empty_zero'] = 0;
  $handler->display->display_options['fields']['column_3']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['column_3']['field_name'] = 'id';
  /* Field: Tweet */
  $handler->display->display_options['fields']['column']['id'] = 'column';
  $handler->display->display_options['fields']['column']['table'] = 'yql';
  $handler->display->display_options['fields']['column']['field'] = 'column';
  $handler->display->display_options['fields']['column']['ui_name'] = 'Tweet';
  $handler->display->display_options['fields']['column']['label'] = '';
  $handler->display->display_options['fields']['column']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['column']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['column']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['column']['alter']['external'] = 1;
  $handler->display->display_options['fields']['column']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['column']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['column']['alter']['alt'] = 'Tweet from [column_1]';
  $handler->display->display_options['fields']['column']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['column']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['column']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['column']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['column']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['column']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['column']['alter']['html'] = 0;
  $handler->display->display_options['fields']['column']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['column']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['column']['hide_empty'] = 0;
  $handler->display->display_options['fields']['column']['empty_zero'] = 0;
  $handler->display->display_options['fields']['column']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['column']['field_name'] = 'text';
  /* Contextual filter: Search string */
  $handler->display->display_options['arguments']['column']['id'] = 'column';
  $handler->display->display_options['arguments']['column']['table'] = 'yql';
  $handler->display->display_options['arguments']['column']['field'] = 'column';
  $handler->display->display_options['arguments']['column']['ui_name'] = 'Search string';
  $handler->display->display_options['arguments']['column']['default_action'] = 'default';
  $handler->display->display_options['arguments']['column']['title_enable'] = 1;
  $handler->display->display_options['arguments']['column']['title'] = 'Comments for !1 on Twitter';
  $handler->display->display_options['arguments']['column']['default_argument_type'] = 'scorg_twitter';
  $handler->display->display_options['arguments']['column']['default_argument_skip_url'] = 0;
  $handler->display->display_options['arguments']['column']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['column']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['column']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['column']['specify_validation'] = 1;
  $handler->display->display_options['arguments']['column']['validate']['type'] = 'scorg_url_encode';
  $handler->display->display_options['arguments']['column']['transform_dash'] = 0;
  $handler->display->display_options['arguments']['column']['field_name'] = 'q';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block_1');
  $handler->display->display_options['block_description'] = 'Tweets about this node';
  $export['scorg_tweets'] = $view;

  return $export;
}
