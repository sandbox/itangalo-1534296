<?php
/**
 * @file
 * scorg_drupal_version.features.inc
 */

/**
 * Implements hook_views_default_views_alter().
 */
function scorg_drupal_version_views_default_views_alter(&$data) {
  if (isset($data['sc_learning_resources'])) {
    $data['sc_learning_resources']->display['default']->display_options['exposed_form']['options']['autosubmit'] = 0; /* WAS: '' */
    $data['sc_learning_resources']->display['default']->display_options['exposed_form']['options']['autosubmit_hide'] = 1; /* WAS: '' */
    $data['sc_learning_resources']->display['default']->display_options['fields']['field_drupal_version'] = array(
      'id' => 'field_drupal_version',
      'table' => 'field_data_field_drupal_version',
      'field' => 'field_drupal_version',
      'alter' => array(
        'alter_text' => 0,
        'make_link' => 0,
        'absolute' => 0,
        'external' => 0,
        'replace_spaces' => 0,
        'trim_whitespace' => 0,
        'nl2br' => 0,
        'word_boundary' => 1,
        'ellipsis' => 1,
        'more_link' => 0,
        'strip_tags' => 0,
        'trim' => 0,
        'html' => 0,
      ),
      'element_label_colon' => 1,
      'element_default_classes' => 1,
      'hide_empty' => 0,
      'empty_zero' => 0,
      'hide_alter_empty' => 1,
      'group_rows' => 1,
      'delta_offset' => '0',
      'delta_reversed' => 0,
      'delta_first_last' => 0,
      'field_api_classes' => 0,
    ); /* WAS: '' */
    $data['sc_learning_resources']->display['default']->display_options['filters']['field_drupal_version_value'] = array(
      'id' => 'field_drupal_version_value',
      'table' => 'field_data_field_drupal_version',
      'field' => 'field_drupal_version_value',
      'group' => 1,
      'exposed' => TRUE,
      'expose' => array(
        'operator_id' => 'field_drupal_version_value_op',
        'label' => 'Drupal version',
        'operator' => 'field_drupal_version_value_op',
        'identifier' => 'version',
        'reduce' => 0,
      ),
    ); /* WAS: '' */
    $data['sc_learning_resources']->display['default']->display_options['filters']['flagged']['group'] = 1; /* WAS: '' */
    $data['sc_learning_resources']->display['default']->display_options['filters']['title_1']['group'] = 1; /* WAS: '' */
    $data['sc_learning_resources']->display['default']->display_options['style_options']['columns']['field_drupal_version'] = 'field_drupal_version'; /* WAS: '' */
    $data['sc_learning_resources']->display['default']->display_options['style_options']['columns']['ops'] = 'count'; /* WAS: 'ops' */
    $data['sc_learning_resources']->display['default']->display_options['style_options']['info']['count']['separator'] = '<br />'; /* WAS: '' */
    $data['sc_learning_resources']->display['default']->display_options['style_options']['info']['field_drupal_version'] = array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ); /* WAS: '' */
    $data['sc_learning_resources']->display['panel_pane_2']->display_options['fields']['field_drupal_version'] = array(
      'id' => 'field_drupal_version',
      'table' => 'field_data_field_drupal_version',
      'field' => 'field_drupal_version',
      'alter' => array(
        'alter_text' => 0,
        'make_link' => 0,
        'absolute' => 0,
        'external' => 0,
        'replace_spaces' => 0,
        'trim_whitespace' => 0,
        'nl2br' => 0,
        'word_boundary' => 1,
        'ellipsis' => 1,
        'more_link' => 0,
        'strip_tags' => 0,
        'trim' => 0,
        'html' => 0,
      ),
      'element_label_colon' => 1,
      'element_default_classes' => 1,
      'hide_empty' => 0,
      'empty_zero' => 0,
      'hide_alter_empty' => 1,
      'group_rows' => 1,
      'delta_offset' => '0',
      'delta_reversed' => 0,
      'delta_first_last' => 0,
      'field_api_classes' => 0,
    ); /* WAS: '' */
    $data['sc_learning_resources']->display['panel_pane_2']->display_options['filters']['field_drupal_version_value'] = array(
      'id' => 'field_drupal_version_value',
      'table' => 'field_data_field_drupal_version',
      'field' => 'field_drupal_version_value',
      'exposed' => TRUE,
      'expose' => array(
        'operator_id' => 'field_drupal_version_value_op',
        'label' => 'Drupal version',
        'operator' => 'field_drupal_version_value_op',
        'identifier' => 'version',
        'reduce' => 0,
      ),
    ); /* WAS: '' */
    unset($data['sc_learning_resources']->display['default']->display_options['exposed_form']['options']['reset_button']);
  }
}
