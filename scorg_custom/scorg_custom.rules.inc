<?php

/**
 * @file
 * Custom Rules stuff for skillcompass.org.
 */

/**
 * Implements hook_rules_action_info().
 */
function scorg_custom_rules_action_info() {
  $actions = array(
    'scorg_get_numbers_into_string' => array(
      'label' => t('Merge numbers into a string'),
      'group' => t('skillcompass.org'),
      'description' => t('Replaces %1, %2 and so on with integers from a list.'),
      'parameter' => array(
        'string' => array(
          'type' => 'text',
          'label' => t('String'),
        ),
        'numbers' => array(
          'type' => 'list<integer>',
          'label' => t('List of numbers'),
        ),
      ),
      'provides' => array(
        'new_string' => array(
          'type' => 'text',
          'label' => t('New string'),
        ),
      ),
    ),
  );
  return $actions;
}

/**
 * Action callback for 'scorg_get_numbers_into_string'.
 * @param $string
 *   The string to insert numbers into.
 * @param $numbers
 *   The list of numbers to insert into the string.
 */
function scorg_get_numbers_into_string($string, $numbers) {
  // Loop through the provided numbers.
  foreach ($numbers as $key => $number) {
    // Replacing starts on %1, but the keys in the array starts on 0. Also, if
    // trying to add a string to a zero-value integer, it's interpreted as an
    // empty string. Thus this extra variable.
    $i = $key + 1;
    $string = str_replace('%' . $i, $number, $string);
  }
  
  return array(
    'new_string' => $string,
  );
}
